---
layout: page
title: Projects
description: ""
group: navigation
---
{% include JB/setup %}

###Some Significant Projects 
* [**Ninja Scraper**](https://github.com/kqdtran/ninjascraper): I extended and modified a scraper that automatically downloaded all past exams for a selected course at Berkeley. It was originally created in Python 2 by another student to work with courses in the CS Department, and I added in several features, such as work with courses in ALL departments, ported to Python 3, etc.

<div class="media">
    <div class="media-left"><img src="/assets/ninja.png" title="Ninja Scraper's Execution"></div>   
    <div class="media-left"><img src="/assets/ninja2.png" title="Ninja Scraper's Result"></div>
</div>

* [**Plagiarism Detection Program**](https://github.com/kqdtran/Plagiarism-Detection): A plagiarism detector that checked for similarity among homework submissions in an Introductory Programming class. Advised by [Professor Tom Rebold](http://tomrebold.com/) of [Monterey Peninsula College](http://www.mpc.edu/Pages/default.aspx). My goal for the summer is to rewrite this program to be object-oriented and more robust. The original works fine and is simpler, but is harder to maintain.

<div class="media">
    <div class="media-left"><img src="/assets/plagiarism.png" title="Plagiarism Detector in Action"></div>   
</div>

* **Database Design Project**: Relational database management system for [Project Juice](http://www.projectjuice.com/), a San Francisco-based startup. [Here's our web presentation slides](./course/ieor115/slide.html).

*Below are other projects that I have the opportunity to work on.*

###School Projects
* **Network Game**: Implements the game [Network](http://www.cs.berkeley.edu/~jrs/61b/hw/pj2/readme) that plays against a human player or another computer program.

* [**Scheme Interpreter**](http://inst.eecs.berkeley.edu/~cs61a/fa12/projects/scheme/scheme.html): Interpreter that parsed and evaluated expressions in a subset of the Scheme language

* [**MapReduce in the Cloud**](http://inst.eecs.berkeley.edu/~cs61c/fa12/projects/01): Text analyzer ran on the Amazon EC2 that determined the co-occurrence rates between phrases in the corpus

* **Spell Checker**: A primitive spell checker that checks and corrects words that are not in a pre-defined dictionary

* [**Maximum Flow on Graphs**](https://github.com/kqdtran/MaxFlowGraph): Implements and Analyzes the Maximum Flow Algorithm to determine the maximum flow through a single-source, single-sink flow network

###Personal Projects
* **This website**: Redesigned during Spring Break 2013. I got the opportunity to play around with Markdown, Bootstrap, and Jekyll during this time. Now I even got a blog to <del>rant</del> share my stories! In the end, every redesign is an opportunity to learn new things :)

* [**Snake**](/games/snake.html): Play the classic game of Snake. I made this when I taught myself some basic Javascript during Spring break. 

* [**Getterbot**](https://github.com/kqdtran/getterbot): A simple chatbot implemented in Python. Extended the work of the original author to Python 3

* [**Algorithms**](https://github.com/kqdtran/ADA1): A collection of popular algorithms implemented in Python and Java when I took the [MOOC](https://en.wikipedia.org/wiki/Massive_open_online_course) [Design and Analysis of Algorithms Part 1 on Coursera](https://www.coursera.org/course/algo)

* **Battle Arena**: A simple role-playing game (RPG), in which characters take turns to attack the opponent or heal himself. Like most RPGs, the goal is to reduce the opponent's health to zero and win the duel

###Research Projects
* **EECS149 Interactive Contents**: Develop interactive contents for EECS149: Introduction to Embedded Systems using LaTeX, Javascript, and MathJax on [Mathapedia](https://mathapedia.com/)

