---
layout: page
title: Khoa Tran
tagline: 
---
{% include JB/setup %}

### Welcome!
Hi there, I'm Khoa and I'm a junior at [UC Berkeley](http://berkeley.edu/) majoring in [Computer Science](http://www.cs.berkeley.edu). I enjoy doing math (especially probability theory), coding, and learning new technology. When I'm not in front of a computer, I play tennis, listen to music, and play tennis while listen to music. Check out my [resume](static/resume.pdf), the [projects](project.html) I've been working on, and/or [email me](mailto:khoatran@berkeley.edu) any questions.     

### Blogging
I occassionally blog about various things I came across. You can find some of my recent blog posts below. 

<ul class="posts">
  {% for post in site.posts %}
    <li><span>{{ post.date | date_to_string }}</span> &raquo; <a href="{{ BASE_PATH }}{{ post.url }}">{{ post.title }}</a></li>
  {% endfor %}
</ul>
