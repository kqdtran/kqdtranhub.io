---
layout: page
title: "Coursework"
description: ""
group: navigation
---
{% include JB/setup %}

Below are some random things I made for some of my classes. Hope this will be helpful to some people! (No, I don't have homework solutions posted here :P)

####CS61B: Data Structures
* [Fall 2006 Final Exam's Draft Solution](./course/cs61b/06final.html)

####CS70: Discrete Mathematics and Probability Theory
* [Midterm 1 Review Questions](./course/cs70/CS70Spr13_study_party.pdf)   
[Solution](./course/cs70/CS70Spr13_study_party_sol.pdf)

####CS170: Efficient Algorithms and Intractable Problems
* [Syllabus + Readings](./course/cs170/s13.html)

####IEOR115: Industrial and Commercial Database Systems 
* [Last lecture (4-29-2013) scribed notes](./course/ieor115/lec4-29.pdf)
* [Final Project's web presentation](./course/ieor115/slide.html)
