---
layout: page
title: About Me
description: My about me page
group: navigation
---
{% include JB/setup %}

###Me, I am a/an:

* Junior, rising senior at [UC Berkeley](http://berkeley.edu/) majoring in [Computer Science](http://www.cs.berkeley.edu)

* International student from Vietnam who came here ~2.5 years ago and started at [Monterey Peninsula College](http://www.mpc.edu/Pages/default.aspx). Graduated with two AA degrees in Math and CS, and transferred to [Cal](http://www.calbears.com/) in Fall 2012

* Reader/Grader worked under [Professor Anant Sahai](http://www.eecs.berkeley.edu/~sahai/) of [CS 70 - Discrete Mathematics and Probability Theory](http://inst.eecs.berkeley.edu/~cs70/sp13/), and continue to work under [Tom Watson](http://www.cs.berkeley.edu/~tom/) for the summer 2k13's iteration of CS 70. 

* Undergraduate Research Apprentice/Content Developer in the [Mathapedia Research Group](https://mathapedia.com/), EECS Department, UC Berkeley

* Big [Manchester United](http://www.manutd.com/Splash-Page.aspx) fan (Go Red Devils!). I love watching English football, playing tennis, frisbee, Fifa, Pokemon, YugiOh, and reading when I have free time. Two of my favorite destinations on the Internet are [Study Hacks](http://calnewport.com/blog/) and [Life Hacks](http://www.marcandangel.com/).
